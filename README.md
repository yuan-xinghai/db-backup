# db-backup

#### 介绍
数据库备份管理系统

#### 技术选型
1. SpringBoot，MySQL，mybatis-plus，quartz，thymeleaf，lombok


#### 安装教程

1. 创建数据库：db-backup，导入/resources/sql/db-backup.sql，修改数据库配置信息
```
spring:
  # Mysql配置
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/db-backup?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Asia/Shanghai&useSSL=true&characterEncoding=UTF-8
    username: root
    password: 123456
```
2. 启动项目，访问：http://localhost:8080/db-backup/

用户名密码在配置文件中
```
# Sa-Token-Quick-Login 配置
sa:
  # 登录账号
  name: 'admin'
  # 登录密码
  pwd: '123456'

```
3. 可在【分区管理】中创建分区，有一个默认分区（此步骤可省略）
4. 可在【实例管理】中添加实例，连接你的数据库信息，暂时只支持MySQL
5. 可在【实例管理】中添加备份，有手动备份及自动备份，可备份整库和单独几张表，可单独备份表结构、表数据、表结构+表数据
6. 可在【任务管理】中查看添加的备份任务
7. 可在【备份记录】中查看备份的日志以及备份文件的下载

#### 功能截图

<img src="https://gitee.com/asurplus/db-backup/raw/master/images/zone.png" width="100%" >
<img src="https://gitee.com/asurplus/db-backup/raw/master/images/nodeInfo.png" width="100%" >
<img src="https://gitee.com/asurplus/db-backup/raw/master/images/backupAdd.png" width="100%" >
<img src="https://gitee.com/asurplus/db-backup/raw/master/images/backupTask.png" width="100%" >
<img src="https://gitee.com/asurplus/db-backup/raw/master/images/backupLog.png" width="100%" >

#### 在线演示

http://1.14.96.198/db-backup/

#### 捐赠

如果觉得还不错，请作者喝杯咖啡吧 ☺

<table>
    <tr>
        <td style="text-align: center">微信扫一扫</td>
        <td style="text-align: center">支付宝扫一扫</td>
    </tr>
    <tr>
        <td><img src="https://img-blog.csdnimg.cn/20210221195740916.png"/></td>
        <td><img src="https://img-blog.csdnimg.cn/20210221195829103.jpg"/></td>
    </tr>
</table>

#### 交流QQ群
群号：<b>916226778</b>

<img src="https://img-blog.csdnimg.cn/20210427105454901.jpg" alt="微信公众号" />

#### 关注

欢迎关注我的微信公众号

<img src="https://img-blog.csdnimg.cn/20210221200018475.jpg" alt="微信公众号" />