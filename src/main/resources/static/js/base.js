/******************************************************** 自己封装的一些常用工具 ********************************************************/

/**
 * 获取上下文环境
 */
window.ctxPath = (function (src) {
    src = document.scripts[document.scripts.length - 1].src;
    return src.substring(0, src.lastIndexOf("/js/") + 1);
})();

/**
 * 去掉字符串的HTML标签
 */
function filterHTMLTag(str) {
    let msg = str.replace(/<\/?[^>]*>/g, '');
    msg = msg.replace(/[|]*\n/, '');
    msg = msg.replace(/&nbsp;/ig, '');
    return msg;
}

var layer;
var table;
var form;
var util;
/**
 * 加载Layui常用组件
 */
layui.use(['layer', 'form', 'util'], function () {
    layer = layui.layer;
    form = layui.form;
    util = layui.util;

    /**
     * 回到顶部
     */
    util.fixbar({});

    /**
     * 鼠标移入，显示弹窗
     */
    let tipsIndex;
    $('body').on('mouseover', 'tbody .layui-table-cell', function () {
        let that = this;
        if (!$(this).hasClass('laytable-cell-numbers') && 0 == $(this).find('.toolbar_span').length && !!filterHTMLTag($(this).html())) {
            tipsIndex = layer.tips(filterHTMLTag($(this).html()), that, {tips: [1, '#1890ff'], time: 5000});
        }
    });

    /**
     * 鼠标移出，关闭弹窗
     */
    $('body').on('mouseout', 'tbody .layui-table-cell', function () {
        if (!$(this).hasClass('laytable-cell-numbers') && 0 == $(this).find('.toolbar_span').length && !!filterHTMLTag($(this).html())) {
            layer.close(tipsIndex);
        }
    });

    /**
     * 双击复制表格内容
     */
    let copyIndex;
    $('body').on('dblclick', 'tbody .layui-table-cell', function () {
        if (!$(this).hasClass('laytable-cell-numbers') && 0 == $(this).find('.toolbar_span').length && !!filterHTMLTag($(this).html())) {
            // 内容
            let text = filterHTMLTag($(this).html());
            // 创建一个input，赋值，选中，赋值，删除input
            let oInput = document.createElement('input');
            oInput.value = text;
            $(oInput).css({opacity: '0'});
            $(oInput).attr({name: "__copy_secukey"});
            document.body.appendChild(oInput);
            // 选择对象
            oInput.select();
            // 复制
            document.execCommand("Copy");
            oInput.className = 'oInput';
            // 删除元素
            $("input[name='__copy_secukey']").remove();
            copyIndex = layer.msg('已复制', {offset: 't'});
        }
    });
});

/**
 * Ajax请求
 */
const Asurplus = {
    /**
     * GET 请求
     */
    get(options, succback, failback, errback) {
        if (!options.url) {
            layer.msg('请求错误', {icon: 2})
            return false;
        }
        options.type = 'GET'
        options.timeout = options.timeout || 10000
        options.async = null == options.async ? true : options.async
        options.cache = null == options.cache ? false : options.cache
        options.dataType = options.dataType || 'json'
        options.contentType = options.contentType || 'application/json'
        options.succMsg = null == options.succMsg ? false : options.succMsg
        options.failMsg = null == options.failMsg ? false : options.failMsg
        options.errMsg = null == options.errMsg ? false : options.errMsg
        let index = layer.load(2, {shade: 0.1});
        $.ajax({
            url: options.url,
            type: options.type,
            timeout: options.timeout,
            async: options.async,
            cache: options.cache,
            dataType: options.dataType,
            // 请求成功回调
            success: function (res) {
                layer.close(index);
                // 业务处理成功
                if (200 === res.code) {
                    // 成功回调
                    if (succback) {
                        succback(res);
                    }
                }
                // 业务处理失败
                else {
                    layer.msg(res.msg, {icon: 2})
                }
            }
        });
    },
    /**
     * POST 请求
     */
    post(options, succback, failback, errback) {
        if (!options.url) {
            layer.msg('请求错误', {icon: 2})
            return false;
        }
        options.type = 'POST'
        options.timeout = options.timeout || 10000
        options.async = null == options.async ? true : options.async
        options.cache = null == options.cache ? false : options.cache
        options.dataType = options.dataType || 'json'
        options.contentType = options.contentType || 'application/json'
        options.data = options.data || ''
        options.succMsg = null == options.succMsg ? false : options.succMsg
        options.failMsg = null == options.failMsg ? false : options.failMsg
        options.errMsg = null == options.errMsg ? false : options.errMsg
        let index = layer.load(2, {shade: 0.1});
        $.ajax({
            url: options.url,
            type: options.type,
            timeout: options.timeout,
            async: options.async,
            cache: options.cache,
            dataType: options.dataType,
            data: options.data,
            contentType: options.contentType,
            // 请求成功回调
            success: function (res) {
                layer.close(index);
                // 业务处理成功
                if (200 === res.code) {
                    // 成功回调
                    if (succback) {
                        succback(res);
                    }
                }
                // 业务处理失败
                else {
                    layer.msg(res.msg, {icon: 2})
                    // 失败回调
                    if (failback) {
                        failback(res);
                    }
                }
            }
        });
    },
    /**
     * 渲染表格
     */
    tableRender(options) {
        if (!options.url) {
            layer.msg('请求错误', {icon: 2})
            return false;
        }
        if (!options.table) {
            layer.msg('请求错误', {icon: 2})
            return false;
        }
        table = options.table
        options.elem = options.elem || '#currentTableId'
        options.id = options.id || 'currentTableId'
        // 最小列宽
        options.cellMinWidth = options.cellMinWidth || 80
        options.method = options.method || 'POST'
        options.toolbar = null == options.toolbar ? '#toolbarDemo' : options.toolbar
        // options.height = options.height || $(document).height() - $('.table-search-fieldset').height() - 80
        options.defaultToolbar = options.defaultToolbar || ['filter', 'exports', 'print']
        options.limits = options.limits || [10, 15, 20, 25, 50, 100]
        options.limit = options.limit || 15
        options.page = null == options.page ? true : options.page
        /**
         * 表格搜索操作
         */
        options.form.on('submit(data-search-btn)', function (data) {
            // 执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: options.form.val("search-form")
            }, 'data');
            return false;
        });

        /**
         * 表格重置搜索
         */
        options.form.on('submit(data-reset-btn)', function (data) {
            // 清空
            $("#search-form")[0].reset();
            // 执行搜索重载
            table.reload('currentTableId', {
                page: {
                    curr: 1
                }
                , where: options.form.val("search-form")
            }, 'data');
            return false;
        });

        /**
         * 表格行单击事件
         */
        table.on('row(currentTableFilter)', function (obj) {
            // 标注选中样式
            obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
        });

        /**
         * 表格行双击事件
         */
        table.on('rowDouble(currentTableFilter)', function (obj) {

        });
        /**
         * 渲染表格
         */
        return options.table.render(options)
    },
    /**
     * 打开新增、修改页面
     */
    openLayer(options, callback) {
        if (!options.content) {
            layer.msg('请求错误', {icon: 2})
            return false;
        }
        options.type = options.type || 2
        options.title = options.title || '新页面'
        options.shadeClose = null == options.shadeClose ? true : options.shadeClose
        options.shade = options.shade || 0.5
        options.area = options.area || ['50%', '100%']
        options.anim = options.anim || 2
        options.move = null != options.move ? options.move : false
        layer.open({
            type: options.type,
            title: options.title,
            shadeClose: options.shadeClose,
            shade: options.shade,
            area: options.area,
            content: options.content,
            offset: options.offset,
            anim: options.anim,
            move: options.move,
            end: function (index, layero) {
                if (!!callback) {
                    callback();
                }
            }
        });
    }
}
