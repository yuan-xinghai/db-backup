package com.asurplus.mapper;

import com.asurplus.entity.BackupTask;
import com.asurplus.common.vo.BackupTaskVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @since 2022-08-04
 */
public interface BackupTaskMapper extends BaseMapper<BackupTask> {

    Page<BackupTaskVO> list(Page<BackupTaskVO> page, @Param(Constants.WRAPPER) Wrapper<BackupTaskVO> queryWrapper);
}
