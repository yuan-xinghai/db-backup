package com.asurplus.mapper;

import com.asurplus.entity.BackupLog;
import com.asurplus.common.vo.BackupLogVO;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @since 2022-08-05
 */
public interface BackupLogMapper extends BaseMapper<BackupLog> {

    Page<BackupLogVO> list(Page<BackupLogVO> page, @Param(Constants.WRAPPER) Wrapper<BackupLogVO> queryWrapper);

    BackupLogVO getBackUpLogVO(Integer id);

    void deleteAll();
}
