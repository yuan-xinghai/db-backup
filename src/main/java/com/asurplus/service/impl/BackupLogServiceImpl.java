package com.asurplus.service.impl;

import com.asurplus.common.enums.BackupCategoryEnum;
import com.asurplus.common.vo.BackupJobVO;
import com.asurplus.entity.BackupLog;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.mapper.BackupLogMapper;
import com.asurplus.service.BackupLogService;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupLogVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @since 2022-08-05
 */
@Service
public class BackupLogServiceImpl extends ServiceImpl<BackupLogMapper, BackupLog> implements BackupLogService {

    @Override
    public LayuiTablePojo list(Integer page, Integer limit, BackupLogVO backUpLogVO) {
        QueryWrapper<BackupLogVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("a.del_flag", 0);
        if (null != backUpLogVO.getZoneId()) {
            queryWrapper.eq("b.zone_id", backUpLogVO.getZoneId());
        }
        if (null != backUpLogVO.getNodeId()) {
            queryWrapper.eq("a.node_id", backUpLogVO.getNodeId());
        }
        if (StringUtils.isNotBlank(backUpLogVO.getDatabaseName())) {
            queryWrapper.like("a.database_name", backUpLogVO.getDatabaseName());
        }
        if (StringUtils.isNotBlank(backUpLogVO.getTablesName())) {
            queryWrapper.like("a.tables_name", backUpLogVO.getTablesName());
        }
        if (StringUtils.isNotBlank(backUpLogVO.getCategory())) {
            queryWrapper.eq("a.category", backUpLogVO.getCategory());
        }
        if (null != backUpLogVO.getStatus()) {
            queryWrapper.eq("a.status", backUpLogVO.getStatus());
        }
        if (StringUtils.isNotBlank(backUpLogVO.getTimeRange())) {
            String stime = backUpLogVO.getTimeRange().split(" - ")[0];
            String etime = backUpLogVO.getTimeRange().split(" - ")[1];
            queryWrapper.ge("a.start_time", completionTimeStart(stime));
            queryWrapper.le("a.end_time", completionTimeEnd(etime));
        }
        queryWrapper.orderByDesc("a.start_time");
        return LayuiTablePojo.ok(this.baseMapper.list(new Page<>(page, limit), queryWrapper));
    }

    /**
     * 保存备份记录
     *
     * @param vo
     * @return
     */
    public BackupLog saveBackupLog(BackupJobVO jobVO, BackupCategoryEnum categoryEnum) {
        // 保存备份记录
        BackupLog backupLog = new BackupLog();
        backupLog.setNodeId(jobVO.getNodeId());
        backupLog.setCategory(categoryEnum.getMsg());
        backupLog.setDatabaseName(jobVO.getDatabase());
        backupLog.setTablesName(StringUtils.join(jobVO.getTables(), ","));
        backupLog.setDataType(jobVO.getDataType());
        backupLog.setIsCompress(jobVO.getIsCompress());
        backupLog.setIsUpload(jobVO.getIsUpload());
        backupLog.setIsDelete(jobVO.getIsDelete());
        backupLog.setStatus(0);
        // 开始时间
        backupLog.setStartTime(new Date());
        this.baseMapper.insert(backupLog);
        return backupLog;
    }

    @Override
    public BackupLogVO getBackUpLogVO(Integer id) {
        return this.baseMapper.getBackUpLogVO(id);
    }

    @Override
    public RES delete(Integer id) {
        this.baseMapper.deleteById(id);
        return RES.ok();
    }

    @Override
    public RES deleteAll() {
        this.baseMapper.deleteAll();
        return RES.ok();
    }

    public static String completionTimeStart(String date) {
        return date + " 00:00:00";
    }

    public static String completionTimeEnd(String date) {
        return date + " 23:59:59";
    }
}
