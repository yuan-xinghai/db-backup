package com.asurplus.service.impl;

import com.asurplus.entity.NodeInfo;
import com.asurplus.entity.ZoneInfo;
import com.asurplus.mapper.NodeInfoMapper;
import com.asurplus.mapper.ZoneInfoMapper;
import com.asurplus.service.ZoneInfoService;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.common.vo.ZoneInfoVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @since 2022-08-05
 */
@Service
public class ZoneInfoServiceImpl extends ServiceImpl<ZoneInfoMapper, ZoneInfo> implements ZoneInfoService {

    @Resource
    private NodeInfoMapper nodeInfoMapper;

    @Override
    public LayuiTablePojo list(Integer page, Integer limit, ZoneInfo reqVO) {
        QueryWrapper<ZoneInfoVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("a.del_flag", 0);
        if (StringUtils.isNotBlank(reqVO.getName())) {
            queryWrapper.like("a.name", reqVO.getName());
        }
        queryWrapper.orderByDesc("a.create_time");
        return LayuiTablePojo.ok(this.baseMapper.list(new Page<>(page, limit), queryWrapper));
    }

    @Override
    public RES add(ZoneInfo zoneInfo) {
        LambdaQueryWrapper<ZoneInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ZoneInfo::getName, zoneInfo.getName());
        int count = this.count(queryWrapper);
        if (0 < count) {
            return RES.no("分区名称已存在");
        }
        this.baseMapper.insert(zoneInfo);
        return RES.ok();
    }

    @Override
    public RES update(ZoneInfo zoneInfo) {
        LambdaQueryWrapper<ZoneInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.ne(ZoneInfo::getId, zoneInfo.getId());
        queryWrapper.eq(ZoneInfo::getName, zoneInfo.getName());
        int count = this.count(queryWrapper);
        if (0 < count) {
            return RES.no("分区名称已存在");
        }
        this.baseMapper.updateById(zoneInfo);
        // 更新分区名称
        NodeInfo nodeInfo = new NodeInfo();
        nodeInfo.setZoneName(zoneInfo.getName());
        LambdaQueryWrapper<NodeInfo> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(NodeInfo::getZoneId, zoneInfo.getId());
        nodeInfoMapper.update(nodeInfo, queryWrapper1);
        return RES.ok();
    }

    @Override
    public RES delete(Integer id) {
        LambdaQueryWrapper<NodeInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(NodeInfo::getZoneId, id);
        Integer row = nodeInfoMapper.selectCount(queryWrapper);
        if (0 < row) {
            return RES.no("该分区下还存在实例，无法删除");
        }
        this.baseMapper.deleteById(id);
        return RES.ok();
    }

    @Override
    public List<ZoneInfo> listSelect() {
        return this.baseMapper.selectList(null);
    }
}
