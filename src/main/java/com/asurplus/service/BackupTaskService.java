package com.asurplus.service;

import com.asurplus.common.vo.BackupTaskUpdateReqVO;
import com.asurplus.entity.BackupTask;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupTaskVO;
import com.asurplus.common.vo.LayuiTablePojo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @since 2022-08-05
 */
public interface BackupTaskService extends IService<BackupTask> {

    LayuiTablePojo list(Integer page, Integer limit, BackupTaskVO backupTask);

    RES update(BackupTaskUpdateReqVO reqVO);

    RES delete(Integer id);

    RES run(Integer id);

    RES updateStatus(BackupTask backupTask);
}
