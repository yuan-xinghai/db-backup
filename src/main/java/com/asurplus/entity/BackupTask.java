package com.asurplus.entity;

import com.asurplus.common.vo.BackupJobVO;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @since 2022-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName(value = "backup_task", autoResultMap = true)
@ApiModel(value = "BackupTask对象", description = "备份任务信息")
public class BackupTask extends Model<BackupTask> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "数据连接id")
    @TableField("node_id")
    private Integer nodeId;

    @ApiModelProperty(value = "cron表达式")
    @TableField("cron")
    private String cron;

    @ApiModelProperty(value = "参数")
    @TableField(value = "param", typeHandler = JacksonTypeHandler.class)
    private BackupJobVO param;

    @ApiModelProperty(value = "状态（0-停止1-启动）")
    @TableField("status")
    private Boolean status;

    @ApiModelProperty(value = "库名")
    @TableField("database_name")
    private String databaseName;

    @ApiModelProperty(value = "表名")
    @TableField("tables_name")
    private String tablesName;

    @ApiModelProperty(value = "备份方式")
    @TableField("data_type")
    private Integer dataType;

    @ApiModelProperty(value = "备注")
    @TableField("remark")
    private String remark;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @ApiModelProperty(value = "删除状态（0--未删除1--已删除）")
    @TableField("del_flag")
    @TableLogic
    private Integer delFlag;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
