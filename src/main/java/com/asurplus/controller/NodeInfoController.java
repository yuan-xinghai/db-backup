package com.asurplus.controller;

import com.asurplus.entity.NodeInfo;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.service.NodeInfoService;
import com.asurplus.service.ZoneInfoService;
import com.asurplus.common.utils.DataSourceUtil;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupReqVO;
import com.asurplus.common.vo.NodeProperties;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @since 2022-08-04
 */
@Controller
@RequestMapping("node-info")
public class NodeInfoController {

    @Autowired
    private NodeInfoService nodeInfoService;
    @Autowired
    private ZoneInfoService zoneInfoService;

    /**
     * 实例管理首页
     */
    @GetMapping("")
    public String list(Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        return "node-info/index";
    }

    /**
     * 实例管理新增页
     */
    @GetMapping("node-info-add")
    public String add(Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        return "node-info/add";
    }

    /**
     * 实例管理修改页
     */
    @GetMapping("node-info-update/{id}")
    public String update(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        model.addAttribute("data", nodeInfoService.getById(id));
        return "node-info/update";
    }

    /**
     * 实例管理备份页
     */
    @GetMapping("node-info-backup/{id}")
    public String backup(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("data", nodeInfoService.getById(id));
        return "node-info/backup";
    }

    /**
     * 实例信息分页查询
     */
    @PostMapping("list")
    @ResponseBody
    public LayuiTablePojo list(Integer page, Integer limit, NodeInfo nodeInfo) {
        return nodeInfoService.list(page, limit, nodeInfo);
    }

    /**
     * 实例信息新增
     */
    @PostMapping("add")
    @ResponseBody
    public RES add(@RequestBody NodeInfo nodeInfo) {
        return nodeInfoService.add(nodeInfo);
    }

    /**
     * 实例信息修改
     */
    @PostMapping("update")
    @ResponseBody
    public RES update(@RequestBody NodeInfo nodeInfo) {
        return nodeInfoService.update(nodeInfo);
    }

    /**
     * 实例信息删除
     */
    @GetMapping("delete/{id}")
    @ResponseBody
    public RES delete(@PathVariable("id") Integer id) {
        return nodeInfoService.delete(id);
    }

    /**
     * 实例信息测试连接
     */
    @PostMapping("connect")
    @ResponseBody
    public RES connect(@RequestBody NodeProperties properties) {
        try (HikariDataSource dataSource = DataSourceUtil.createDataSource(properties)) {
            if (dataSource.isRunning()) {
                return RES.ok("连接成功");
            }
        } catch (Exception e) {
            return RES.no("连接失败：" + e.getMessage());
        }
        return RES.no("连接失败");
    }

    /**
     * 获取实例信息数据库列表
     */
    @PostMapping("listDatabases")
    @ResponseBody
    public RES listDatabases(@RequestBody NodeProperties properties) {
        try (HikariDataSource dataSource = DataSourceUtil.createDataSource(properties)) {
            if (dataSource.isRunning()) {
                List<String> strings = DataSourceUtil.listDataBases(dataSource);
                return RES.ok("操作成功", strings);
            }
        } catch (Exception e) {
            return RES.no("连接失败：" + e.getMessage());
        }
        return RES.no("获取数据库信息失败");
    }

    /**
     * 获取数据库所有表信息
     */
    @PostMapping("listTables")
    @ResponseBody
    public RES listTables(@RequestBody NodeProperties properties) {
        try (HikariDataSource dataSource = DataSourceUtil.createDataSource(properties)) {
            if (dataSource.isRunning()) {
                List<String> strings = DataSourceUtil.listTables(dataSource);
                return RES.ok("操作成功", strings);
            }
        } catch (Exception e) {
            return RES.no("连接失败：" + e.getMessage());
        }
        return RES.no("获取数据表信息失败");
    }

    /**
     * 保存备份（立即备份，备份任务）
     */
    @PostMapping("saveBackUp")
    @ResponseBody
    public RES saveBackUp(@RequestBody BackupReqVO reqVO) {
        return nodeInfoService.saveBackUp(reqVO);
    }
}
