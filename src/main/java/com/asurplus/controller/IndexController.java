package com.asurplus.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class IndexController {

    /**
     * 首页
     */
    @GetMapping({"", "/", "index"})
    public String index() {
        return "index";
    }

    /**
     * 欢迎页
     */
    @GetMapping("welcome")
    public String welcome() {
        return "welcome";
    }

    /**
     * 登出
     */
    @GetMapping("doLogout")
    public String doLogout(HttpServletRequest request, HttpServletResponse response) {
        // 删除cookie
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                cookie.setPath("/");
                cookie.setMaxAge(-1);
                cookie.setValue(null);
                response.addCookie(cookie);
            }
        }
        return "redirect:";
    }
}
