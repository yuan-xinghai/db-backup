package com.asurplus.common.minio;

/**
 * minio常量类
 *
 * @author asurplus
 */
public final class MinioConstant {

    /**
     * 存储桶
     */
    public static class Bucket {

        /**
         * 系统端默认存储桶
         */
        public static final String DEFAULT_BUCKET = "db-backup-bucket";
    }

    /**
     * 存储目录
     */
    public static class Dir {

        /**
         * 系统端默认目录
         */
        public static final String SYS_DEFAULT = "sys_default";
    }
}
